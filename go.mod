module gitlab.com/kotahkon/consumer

go 1.13

require (
	github.com/gin-gonic/gin v1.6.2
	github.com/jasonlvhit/gocron v0.0.0-20200323211822-1a413f9a41a2
	github.com/lib/pq v1.3.0
	github.com/nsqio/go-nsq v1.0.8
	github.com/vmihailenco/msgpack v4.0.4+incompatible
	gitlab.com/kotahkon/utilities v1.2.1
)
