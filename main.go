package main

import (
	"log"

	"gitlab.com/kotahkon/consumer/service"
)

func main() {
	s, err := service.New()
	if err != nil {
		log.Fatal(err)
	}
	s.Run()
}
