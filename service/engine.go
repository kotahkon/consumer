package service

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/kotahkon/utilities/cache"
	"gitlab.com/kotahkon/utilities/database"
)

func (s *Service) newEngine() error {
	gin.SetMode(gin.ReleaseMode)
	s.engine = gin.New()
	s.engine.NoMethod(func(ctx *gin.Context) {
		ctx.JSON(http.StatusMethodNotAllowed, gin.H{
			"status":  "method-not-allowed",
			"message": "Method Not Allowed",
		})
	})
	s.engine.NoRoute(func(ctx *gin.Context) {
		ctx.JSON(http.StatusNotFound, gin.H{
			"status":  "not-found",
			"message": "Route Not Found",
		})
	})

	s.engine.GET("/status", func(ctx *gin.Context) {
		err := cache.Ping()
		if err != nil {
			ctx.String(http.StatusInternalServerError, "status: cache ping error")
			return
		}

		err = database.Get().DB().Ping()
		if err != nil {
			ctx.String(http.StatusInternalServerError, "status: database ping error")
			return
		}
		ctx.String(http.StatusOK, "status: ok")
	})
	return nil
}
