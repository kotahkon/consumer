package service

import (
	"time"

	"github.com/nsqio/go-nsq"
	"github.com/vmihailenco/msgpack"

	"gitlab.com/kotahkon/utilities/browser"
	"gitlab.com/kotahkon/utilities/database"
)

var (
	consumer *nsq.Consumer
)

func (s *Service) HandleMessage(message *nsq.Message) error {
	var input map[string]interface{}
	err := msgpack.Unmarshal(message.Body, &input)
	if err != nil {
		return err
	}

	targetID := input["target_id"].(uint64)
	ipAddress := input["ip_address"].(string)
	userAgent := input["user_agent"].(string)
	createdAt := input["created_at"].(*time.Time)

	userAgentInfo := browser.Parse(userAgent)

	visitRecord := &database.VisitRecord{}
	visitRecord.CreatedAt = *createdAt
	visitRecord.IPAddress = ipAddress
	visitRecord.TargetID = uint(targetID)
	visitRecord.Browser = userAgentInfo
	return database.Model(visitRecord).Create(visitRecord).Error
}

func (s *Service) newConsumer(nsqdAddr, topic, channel string) error {
	c, err := nsq.NewConsumer(topic, channel, nsq.NewConfig())
	if err != nil {
		return err
	}
	c.AddConcurrentHandlers(s, 10)
	consumer = c
	return c.ConnectToNSQD(nsqdAddr)
}
