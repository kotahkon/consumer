package service

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/jasonlvhit/gocron"
	_ "github.com/lib/pq"

	"gitlab.com/kotahkon/utilities/cache"
	"gitlab.com/kotahkon/utilities/database"
)

type Service struct {
	engine *gin.Engine
}

func New() (*Service, error) {
	var err error

	s := new(Service)
	err = s.newEngine()
	if err != nil {
		return nil, err
	}

	err = database.Open(os.Getenv("DATABASE_CONNECTION"))
	if err != nil {
		return nil, err
	}

	cache.New(os.Getenv("REDIS_ADDR"))
	err = cache.Ping()
	if err != nil {
		return nil, err
	}

	err = s.newConsumer(os.Getenv("NSQ_CONNECTION"), "visit", "analytics_channel")
	if err != nil {
		return nil, err
	}

	// TODO: use better job scheduler
	gocron.Every(5).Minutes().Do(s.updateVisits)

	return s, nil
}

func (s *Service) updateVisits() {
	log.Println("Updating column visits of targets ...")
	type Counter struct {
		ID    uint
		Count uint
	}
	targets := []Counter{}
	database.Get().Raw(`
		select targets.id, count(visits.id) 
		from visit_records as visits, targets 
		where targets.id = visits.target_id 
		group by targets.id;
	`).Scan(&targets)

	for _, target := range targets {
		database.Get().Exec(`update targets set visits = ? where id = ?`, target.Count, target.ID)
	}
}

func (s *Service) Run() {
	srv := &http.Server{
		Addr:    ":8091",
		Handler: s.engine,
	}
	go func() {
		log.Println("Listening on :8091 ...")
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()
	go func() {
		<-gocron.Start()
	}()

	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit

	log.Println("Shuting down server...")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	consumer.Stop()
	gocron.Clear()

	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server forced to shutdown:", err)
	}
	log.Println("Server exiting")
}
